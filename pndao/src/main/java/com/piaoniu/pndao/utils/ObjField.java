package com.piaoniu.pndao.utils;

/**
 * Field
 *
 * @author T_T
 * @since 10/12/2016 09:24
 */
public class ObjField {
    private String value;

    private String type;//snippet,express,column

    private String columnStyle;

    private String operator;

    public ObjField(String value, String type, String columnStyle, String operator) {
        this.value = value;
        this.type = type;
        this.columnStyle = columnStyle;
        this.operator = operator;
    }

    @Override
    public String toString() {

        if ("column".equals(type)) {
            return SqlBuilder.column(value, columnStyle);
        }

        if ("snippet".equals(type)) {
            return value;
        }

        if ("condition".equals(type)) {
            return SqlBuilder.conditionExpress(value, operator, columnStyle);
        }

        if ("express".equals(type)) {
            return SqlBuilder.express(value);
        }

        if ("order".equals(type)) {
            return value + " " + operator;
        }

        throw new IllegalArgumentException();
    }

    public static ObjField column(String value, String columnStyle) {
        return new ObjField(
                value, "column", columnStyle, null
        );
    }

    public static ObjField express(String value) {
        return new ObjField(
                value, "express", null, null
        );
    }

    public static ObjField snippet(String value) {
        return new ObjField(
                value, "snippet", null, null
        );
    }

    public static ObjField condition(String value, String operator, String columnStyle) {
        return new ObjField(
                value, "condition", columnStyle, operator
        );
    }

    public static ObjField condition(String value) {
        return condition(value, "=", "underline");
    }

    public static ObjField order(String field, String order) {
        return new ObjField(
                field, "order", null, order
        );
    }

    public static ObjField order(String field) {
        return new ObjField(
                field, "order", null, "ASC"
        );
    }

    public static void main(String[] args) {
        System.out.println(column("helloWorld", "underline"));
        System.out.println(snippet("*"));
        System.out.println(condition("HelloWorld", ">=", "underline"));
        System.out.println(condition("helloWorld"));
    }
}
