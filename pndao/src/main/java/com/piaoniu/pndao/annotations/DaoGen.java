package com.piaoniu.pndao.annotations;

public @interface DaoGen {
    String[] insertPrefix() default {"insert"};

    String[] batchInsertPrefix() default {"batchInsert"};

    String[] findPrefix() default {"findBy"};

    String[] removePrefix() default {"removeBy"};

    String[] queryPrefix() default {"queryBy"};

    String[] queryInPrefix() default {"queryIn"};

    String[] updatePrefix() default {"update"};

    String[] updateForPrefix() default {"updateFor"};

    String[] countPrefix() default {"countBy"};

    String[] countInPrefix() default {"countIn"};

    String[] countAllPrefix() default {"countAll"};

    String[] queryAllPrefix() default {"queryAll"};

    String separator() default "And";

    String tablePrefix() default "T_";

    String primaryKey() default "id";

    String createTime() default "createTime";

    String updateTime() default "updateTime";

    String tableName() default "";

    String orderBy() default "OrderBy";

    String orderByWith() default "With";

    String daoSuffix() default "Dao";

    String columnStyle() default "underline";//Camel,Underline

    String[] searchPrefix() default {"search"};

    String[] countSearchPrefix() default {"countSearch"};

    String searchConfig() default "";//like:username,password;between:createTime;ignore:id;not:id;

}
