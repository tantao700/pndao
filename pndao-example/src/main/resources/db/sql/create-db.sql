CREATE TABLE T_user_name (
  id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `user_name` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `mobile_no` varchar(200) NOT NULL,
  `avatar` VARCHAR(255) NOT NULL ,
  `create_time` datetime NOT NULL,
  `update_time` datetime NOT NULL
);