package com.piaoniu.user.entity;

import com.piaoniu.common.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author code4crafter@gmail.com
 *         Date: 16/10/30
 *         Time: 下午4:57
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UserName extends Entity {

    private String userName;

    private String password;

    private String mobileNo;

    private String avatar;
}
