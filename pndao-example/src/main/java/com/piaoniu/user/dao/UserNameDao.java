package com.piaoniu.user.dao;

import com.piaoniu.common.EntityDao;
import com.piaoniu.pndao.annotations.DaoGen;
import com.piaoniu.user.entity.UserName;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.RowBounds;

import java.util.List;
import java.util.Map;

/**
 * @author code4crafter@gmail.com
 *         Date: 16/10/30
 *         Time: 下午4:59
 */
@DaoGen
public interface UserNameDao extends EntityDao<UserName> {

    List<UserName> queryByUserName(@Param("userName") String userName, RowBounds rowBounds);

    int updateForUserName(@Param("userName") String userName, @Param("id") int id);

    int countByUserName(@Param("userName") String userName);

    List<UserName> searchBy(@Param("criteria") Map<String,Object> criteria, RowBounds rowBounds);

    int countSearchBy(@Param("criteria") Map<String,Object> criteria);

}
